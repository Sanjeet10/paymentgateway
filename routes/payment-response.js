const express = require('express');
const router = express.Router();
const axios = require('axios')
const querystring = require('querystring');
var createError = require('http-errors');
router.post('/', function (req, res, next) {
  processResponse(req, res, next);
});
router.get('/', function (req, res, next) {
  processResponse(req, res, next);
});
function processResponse(req, res, next) {
  const reqId = req.query.reqid || req.body.reqid;
  const clientRef = req.query.clientRef || req.body.clientRef;
  const BASE_URL = 'https://sampath.paycorp.lk/webinterface/qw/confirm';
  const AUTH_TOKEN = '8b19c7c7-4208-4934-8865-5f037ad7d15c';
  axios.post(`${BASE_URL}?csrfToken=${reqId}&authToken=${AUTH_TOKEN}`, {})
    .then(function (response) {
      const respData = querystring.parse(response.data);
      respData.reqid = req.query.reqid;
      console.log('RESPONSE DATA:', JSON.stringify(respData));
      // res.render('success', {
      //   title:  'Payment Successfully',
      //   data: respData 
      // });
      if (respData.responseCode == "00" || respData.responseCode == 00) {
        res.redirect("https://api.webdevelopmentsolution.net/ondemand/success")
        // res.status(200).send({
        //   title: 'Payment Successfully',
        //   data: "https://api.webdevelopmentsolution.net/ondemand/success"
        // })
      } else {
        res.render('payment-response', {
          title: 'Payment Response',
          data: respData
        });
      }

    })
    .catch(function (error) {
      console.log('ERROR:', error);
      next(createError(error.response.status, error.response.statusText));
    });
}
module.exports = router;