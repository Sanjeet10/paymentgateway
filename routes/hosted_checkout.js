var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('hosted_checkout', {
    title:  'Hosted Checkout'
  });
});

module.exports = router;
