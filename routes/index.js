var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index', {
    title:  `Bob's Hardware`
  });
});

module.exports = router;
