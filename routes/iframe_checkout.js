var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('iframe_checkout', {
    amount: req.query.id,
    userId:req.query.userId,
    clientRef:req.query.clientRef
  });
});

module.exports = router;
